#!/usr/bin/env bash

install2.r \
    shinylive \
   shiny \
   ordinal \
   DT \
   readxl \
   plotrix \
   dplyr \
   shinyBS \
   shinyscreenshot \
   ggplot2 \
   writexl 

mkdir -p out

r -e 'shinylive::export("./mmi_rank",".public")'
