# MMI Interview App

This is a shiny app that can run as a "shinylive" app within the browser so no server is required and
no personal data leaves the local computer.

The `.gitlab-ci.yml` file builds a Gitlab Pages web site that can be opened to start the application.

To build the application locally, clone the repo and install the required packages for the application
as well as `shinylive`. You can run the application locally as a shiny app using `shiny::runApp` but
this will run as a local server. This should launch quicker than as a `shinylive` application so that's
recommended for testing.

To build as a `shinylive` app use `shinylive::export("./mmi_rank/","./build/")`  and then start a server
to serve the folder as a static web site. This should then run the application.

Loading the application from a static server may take some time as a full web-compiled version of
R needs to be downloaded so that it can all run in the browser. If you suspect the system has 
hung up and have waited a couple of minutes, check the browser console for error messages, and also
try running as a shiny server process as well, since this may reveal more debugging messages.


